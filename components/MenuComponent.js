import React, {Component} from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import { SPORTS } from '../shared/sports';

class Menu extends Component {
    constructor(props){
        super(props);
        this.state = {
            sports: SPORTS
        };
    }

    static navigationOptions = {
        title: 'Sports'
    };
    
    mapSports = sports => {
        const { navigate } = this.props.navigation;

        return sports.map((item, i) => 
            <Card
                key={i}
                featuredTitle={item.name}
                image={require('../assets/img/futbol.jpg')}>
                <Button
                icon={<Icon name='search' color='#ffffff' />}
                backgroundColor='#03A9F4'
                fontFamily='Lato'
                buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                title='Search'
                onPress={() => navigate('Sites', { siteId: item.id })}/>
            </Card>
        );
    };

    render(){
        const { sports } = this.state;

        return (
            <ScrollView>
                <View style={{ display: 'flex' ,flexWrap: 'wrap' }}>
                    {this.mapSports(sports)}
                </View>
            </ScrollView>
        );
    }
}

export default Menu;