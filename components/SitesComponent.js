import React, {Component} from 'react';
import { ScrollView, View, StyleSheet, Modal } from 'react-native';
import { Card, Text, Badge, Button, Icon, Overlay } from 'react-native-elements';
import { SITES } from '../shared/sites';

class Sites extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            sites: SITES,
            isVisible:false
        };
    }

    static navigationOptions = {
        title: 'Available Sites'
    };

    showOverlay = () => {
        this.setState({
            isVisible: true,
        });
    };

    mapSites = sports => {
        return sports.map((item, i) => 
            <Card
                key={i}
                title={item.name}
                image={require('../assets/img/cancha.jpg')}>
                <Text></Text>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Icon 
                        name='location'
                        type='evilicon'/>
                    <Text>{item.direction}</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Icon 
                        name='md-alarm'
                        type='ionicon'/>
                    {
                        this.mapSchedules(item.schedule)
                    }
                </View>
                <Button
                    icon={
                        <Icon
                        name='md-arrow-round-forward'
                        type='ionicon'
                        color='white'
                        />
                    }
                    title={'FROM '+ item.miniumCost}
                    iconRight
                    buttonStyle={styles.RoundButtonStyle}
                    onPress={this.showOverlay}
                    />
            </Card>
        );
    };

    mapSchedules = schedules => {
        return schedules.map((item, i) => 
            <Badge
                key={i} 
                containerStyle={{ backgroundColor: 'violet'}}>
                <Text>{item.days +' '+ item.hours}</Text>
            </Badge>
        );
    };

    render(){
        const { sites } = this.state;         
        const siteId = this.props.navigation.getParam('siteId','');

        console.log("cddcdcd: ",siteId);
        return (
            <View>
                <ScrollView>
                    <View>
                        {
                            this.mapSites(sites.filter((dish)=> dish.id_sport === siteId))
                        }
                    </View>
                </ScrollView>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.isVisible}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <View style={{marginTop: 22}}>
                        <View>
                            <Text>Hello World!</Text>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
 
    RoundButtonStyle: {
      marginTop:10,
      paddingTop:5,
      paddingBottom:5,
      marginLeft:10,
      marginRight:10,
      backgroundColor:'#512DA8',
      borderRadius:15,
      borderWidth: 1,
      borderColor: '#fff'
    }   
});

export default Sites;