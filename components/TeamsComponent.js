import React, {Component} from 'react';
import { ScrollView,View, Text , FlatList} from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import { TEAMS } from '../shared/teams';
import { CONTACTS } from '../shared/contacts';


class Teams extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            teams: TEAMS,
            contacts: CONTACTS 
        }
    }

    static navigationOptions = {
        title: 'My Teams'
    };

    render(){
        const renderTeamsItem = ({item, index}) => {

            return (
                <ListItem
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    hideChevron={true}
                    leftAvatar={{ source: require('./images/uthappizza.png')}}
                    />
            );
        };

        const renderTeamsItem = ({item, index}) => {

            return (
                <ListItem
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    hideChevron={true}
                    leftAvatar={{ source: require('./images/uthappizza.png')}}
                    />
            );
        };

        return(
                <ScrollView style={{ flex: 1 }}>
                    <RenderHistory />
                    <Card
                        title='Corporate Leadership'>
                        <FlatList 
                            data={this.state.leaders}
                            renderItem={renderLeadersItem}
                            keyExtractor={item => item.id.toString()}
                        />
                    </Card>
                </ScrollView>
        );
    }
}

export default Teams;