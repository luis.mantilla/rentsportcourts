export const SPORTS =
    [
        {
            id: 0,
            name:'Fútbol',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        },
        {
            id: 1,
            name:'Voleibol',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        },
        {
            id: 2,
            name:'Tenis',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        },
        {
            id: 3,
            name:'Equitación',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        },
        {
            id: 4,
            name:'Bolos',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        },
        {
            id: 5,
            name:'Billar',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        },
        {
            id: 6,
            name:'Baloncesto',
            category: 'Deportes de equipo',
            image: '',
            status: true                    
        }
    ];