export const SITES =
    [
        {
            id: 0,
            id_sport: 0,
            name:'La Cancha',
            direction: 'Av. Ayacucho #12',
            lat:-16.15798465,
            lng:-68.54545478,
            schedule: [{days:'Mon - Fri', hours:'9:00 - 23:00'},{days:'Sat', hours:'11:00 - 20:00'}],
            image: '',
            miniumCost: '16 Bs/H',
            status: true                    
        },
        {
            id: 1,
            id_sport: 0,
            name:'La revancha',
            direction: 'Av. Ayacucho #12',
            lat:-16.15798465,
            lng:-68.54545478,
            schedule: [{days:'Mon - Fri', hours:'9:00 - 23:00'},{days:'Sat', hours:'11:00 - 20:00'}],
            image: '',
            miniumCost: '10 Bs/H',
            status: true                    
        },
        {
            id: 2,
            id_sport: 1,
            name:'La Cancha 2',
            direction: 'Av. Ayacucho #12',
            lat:-16.15798465,
            lng:-68.54545478,
            schedule: [{days:'Mon - Fri', hours:'9:00 - 23:00'},{days:'Sat', hours:'11:00 - 20:00'}],
            image: '',
            miniumCost: '18 Bs/H',
            status: true                    
        },
        {
            id: 3,
            id_sport: 2,
            name:'La Cancha 3',
            direction: 'Av. Ayacucho #12',
            lat:-16.15798465,
            lng:-68.54545478,
            schedule: [{days:'Mon - Fri', hours:'9:00 - 23:00'},{days:'Sat', hours:'11:00 - 20:00'}],
            image: '',
            miniumCost: '13 Bs/H',
            status: true                    
        }
    ];